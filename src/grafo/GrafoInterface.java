/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package grafo;

/**
 * Contrato de Grafo
 * @author doanvelagui
 * @param <N>, tipo de dato de los nodos
 */
public interface GrafoInterface<N> {
	
	/**
	 * Eval�a si el grafo est� vac�o
	 * @return true si esta vacio, false si no lo est�
	 */
	public boolean isEmpty();
	
	/**
	 * retorna el n�mero de nodos del Grafo
	 * @return nodes, n�mero de nodos del grafo
	 */
	public int sizeNodes();
	
	/**
	 * Retorna el n�mero de enlaces del grafo
	 * @return edges, n�mero de enlaces del grafo
	 */
	public int sizeEdges();
	
	/**
	 * A�ade el nodo recibido como parametro.
	 * @param node, nodo a agregar
	 * @return true si el nodo es agregado, false si no lo es
	 */
	public boolean addNode(N node);
	
	/**
	 * A�ade un enlace del primer nodo, recibido como par�metro, al segundo.
	 * @param node1, origen del enlace.
	 * @param node2, destino del enlace.
	 * @return true si el enlace es agregado, false si no lo es
	 */
	public boolean addEdge(N node1, N node2, int label);
}