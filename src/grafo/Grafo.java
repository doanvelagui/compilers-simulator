/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package grafo;

import java.util.*;

public class Grafo implements GrafoInterface<Integer>{
	
	// CONSTANTES
	
	public static int CAPACIDAD = 20;
	
	// VARIABLES LOCALES
	public int estadoInicial;
	public ArrayList<Integer> estadosFinales;
	
	// VARIABLES
	
	/**
	 * Tabla de adjacencia
	 */
	protected HashMap<Integer,LinkedList<Enlace<Integer>>> adjacencyTable;
	
	// METODOS
	
	/**
	 * Constructor de la clase
	 */
	public Grafo(){
		adjacencyTable = new HashMap<Integer,LinkedList<Enlace<Integer>>>(CAPACIDAD);
		estadosFinales = new ArrayList<Integer>();
	}

	/**
	 * Eval�a si el grafo est� vac�o
	 * @return true si esta vacio, false si no lo est�
	 */
	public boolean isEmpty() {
		return adjacencyTable.isEmpty();
	}

	/**
	 * retorna el n�mero de nodos del Grafo
	 * @return nodes, n�mero de nodos del grafo
	 */
	public int sizeNodes() {
		return adjacencyTable.size();
	}
	
	/**
	 * Retorna el n�mero de enlaces del grafo
	 * @return edges, n�mero de enlaces del grafo
	 */
	public int sizeEdges() {
		int edges = 0;
		Integer[] keys = getNodos();
		for (int i=0;i<CAPACIDAD;i++){ 
			if (keys[i] != null){ 
				LinkedList<Enlace<Integer>> enlaces = adjacencyTable.get(keys[i]); 
				edges += enlaces.size();
			}
		}
		return edges;
	}
	
	/**
	 * A�ade el nodo recibido como parametro.
	 * @param node, nodo a agregar
	 * @return true si el nodo es agregado, false si no lo es
	 */
	public boolean addNode(Integer node) {
		if (adjacencyTable.containsKey(node)) 
            return false; 
		adjacencyTable.put(node, new LinkedList<Enlace<Integer>>()); 
		return true;
	}
	
	/**
	 * A�ade un enlace del primer nodo, recibido como par�metro, al segundo.
	 * @param node1, origen del enlace.
	 * @param node2, destino del enlace.
	 * @return true si el enlace es agregado, false si no lo es
	 */
	public boolean addEdge(Integer node1, Integer node2, int label) {
		if (!adjacencyTable.containsKey(node1)) 
			addNode(node1);
		if (!adjacencyTable.containsKey(node2)) 
			addNode(node2);
		LinkedList<Enlace<Integer>> list = adjacencyTable.get(node1);
		Enlace<Integer> enlace = new Enlace<Integer>(node2, label);
		if(list.contains(enlace))
			return false;
		else{
	        list.add(enlace);
			return true;
		}
	}
	
	/**
	 * Retorna los nodos del grafo
	 * @return Array de nodos
	 */
	public Integer[] getNodos(){
		Set<Integer> setio = adjacencyTable.keySet();
		Integer[] retornar = new Integer[setio.size()];
		setio.toArray(retornar);
		return retornar;
	}
	/**
	 * Retorna los enlaces de un nodo
	 * @return LinkedList de enlaces del nodo
	 */
	public LinkedList<Enlace<Integer>> getEdges(int node){
		return adjacencyTable.get(node);
	}
}
