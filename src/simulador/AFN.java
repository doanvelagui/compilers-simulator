/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package simulador;

import java.util.*;
import grafo.*;
/**
 * Clase constructora del Automata Finito No determinista basado en el 
 * algoritmo de Thompson a partir de una Expresion Regular
 * @author doanvelagui
 */
public class AFN {

	// CONSTANTES
	private final static int cat = 8226;
	private final static int pip = 124;
	private final static int ast = 42;
	private final static int eps = 248;
	
	// VARIABLES GLOBALES
	protected static int Estado = 1;
	
	// VARIABLES LOCALES
	protected Grafo automata;
	
	/**
	 * Constructor de la clase
	 * @param r, expresion regular asociada
	 * @param alfabeto, alfabeto asociado a r
	 */
	public AFN(ExpresionRegular expresionRegular, ArrayList<Integer> alfabeto){
		automata = Thompson(expresionRegular, alfabeto);
	}
	/**
	 * Construye el automata finito no determinista basado en el algoritmo de Thompson
	 * @param er
	 * @param alf
	 * @return grafo representante del AFN
	 */
	private Grafo Thompson(ExpresionRegular er, ArrayList<Integer> alf){
		if(er!=null){
			Grafo grafo = new Grafo();
			//Caso base
			if(alf.contains(er.darSimbolo()) || er.darSimbolo()==eps){
				int nodo1 = Estado; Estado++;
				int nodo2 = Estado; Estado++;
				grafo.estadoInicial=nodo1;
				grafo.estadosFinales.add(nodo2);
				grafo.addEdge(nodo1, nodo2, er.darSimbolo());
			}
			//Concatenacion
			else if(er.darSimbolo()==cat){
				Grafo g1 = Thompson(er.darIzquierda(), alf);
				Grafo g2 = Thompson(er.darDerecha(), alf);
				
				grafo.estadoInicial=g1.estadoInicial;
				for(int n=0; n<g2.estadosFinales.size(); n++)
					if(!grafo.estadosFinales.contains(g2.estadosFinales.get(n)))
						grafo.estadosFinales.add(g2.estadosFinales.get(n));
				
				for(int n=0;n<g1.getNodos().length;n++){
					LinkedList<Enlace<Integer>> edges = g1.getEdges(g1.getNodos()[n]);
					while(edges!=null && !edges.isEmpty()){
						Enlace<Integer> e = edges.remove();
						
						for(int m=0; m<g1.estadosFinales.size(); m++){
							int n1 = (g1.getNodos()[n]==g1.estadosFinales.get(m)) ? g2.estadoInicial : g1.getNodos()[n];
							int n2 = (e.getDestino()==g1.estadosFinales.get(m)) ? g2.estadoInicial : e.getDestino();
							grafo.addEdge(n1, n2, e.getPeso());
						}
					}	
				}
				
				for(int n=0;n<g2.getNodos().length;n++){
					LinkedList<Enlace<Integer>> edges = g2.getEdges(g2.getNodos()[n]);
					while(edges!=null && !edges.isEmpty()){
						Enlace<Integer> e = edges.remove();
						grafo.addEdge(g2.getNodos()[n], e.getDestino(), e.getPeso());
					}
				}
			}
			//OR
			else if(er.darSimbolo()==pip){
				Grafo g1 = Thompson(er.darIzquierda(), alf);
				Grafo g2 = Thompson(er.darDerecha(), alf);
				grafo.estadoInicial=Estado; Estado++;
				grafo.estadosFinales.add(Estado); Estado++;
				
				for(int n=0;n<g1.getNodos().length;n++){
					LinkedList<Enlace<Integer>> edges = g1.getEdges(g1.getNodos()[n]);
					while(edges!=null && !edges.isEmpty()){
						Enlace<Integer> e = edges.remove();
						grafo.addEdge(g1.getNodos()[n], e.getDestino(), e.getPeso());
					}	
				}
				
				for(int n=0;n<g2.getNodos().length;n++){
					LinkedList<Enlace<Integer>> edges = g2.getEdges(g2.getNodos()[n]);
					while(edges!=null && !edges.isEmpty()){
						Enlace<Integer> e = edges.remove();
						grafo.addEdge(g2.getNodos()[n], e.getDestino(), e.getPeso());
					}
				}
				
				grafo.addEdge(grafo.estadoInicial, g1.estadoInicial, eps);
				grafo.addEdge(grafo.estadoInicial, g2.estadoInicial, eps);
				
				for(int n=0;n<grafo.estadosFinales.size();n++){
					for(int m=0;m<g1.estadosFinales.size();m++)
						grafo.addEdge(g1.estadosFinales.get(m), grafo.estadosFinales.get(n), eps);
					for(int m=0;m<g2.estadosFinales.size();m++)
						grafo.addEdge(g2.estadosFinales.get(m), grafo.estadosFinales.get(n), eps);
				}
			}
			//Cerradura de Kleene
			else if(er.darSimbolo()==ast){
				grafo = Thompson(er.darIzquierda(), alf);
				int nuevoEstadoInicial=Estado; Estado++;
				int nuevoEstadoFinal=Estado; Estado++;
				
				grafo.addEdge(nuevoEstadoInicial, grafo.estadoInicial, eps);
				for(int n=0;n<grafo.estadosFinales.size();n++)
					grafo.addEdge(grafo.estadosFinales.get(n), nuevoEstadoFinal, eps);
				
				grafo.addEdge(nuevoEstadoInicial, nuevoEstadoFinal, eps);
				grafo.addEdge(nuevoEstadoFinal, nuevoEstadoInicial, eps);
				
				grafo.estadosFinales = new ArrayList<Integer>(); 
				grafo.estadosFinales.add(nuevoEstadoFinal);
				grafo.estadoInicial = nuevoEstadoInicial;
			}
			return grafo;
		}
		else
			return null;
	}
	/**
	 * Retorna el AFN de la clase
	 * @return AFN, Automata Finito No determinista
	 */
	public Grafo darAutomata(){
		return automata;
	}
}
