/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package simulador;

import java.util.*;
import grafo.*;

public class AFD {
	
	// CONSTANTES
	private final static int eps = 248;
	
	// VARIABLE LOCAL
	protected Grafo automata;
	
	/**
	 * Constructor de la clase
	 * @param afn, automata finito no determinista
	 * @param alfabeto, alfabeto asociado al afn
	 */
	public AFD(AFN afn, ArrayList<Integer> alfabeto){
		automata = (afn==null) ? null: Subconjuntos(afn.automata, alfabeto);
	}
	
	/**
	 * Construye el automata finito no determinista basado en el algoritmo de Thompson
	 * @param er
	 * @param alf
	 * @return grafo representante del AFN
	 */
	private Grafo Subconjuntos(Grafo automata, ArrayList<Integer> alfabeto){
		Grafo nuevoAutomata = new Grafo();
		nuevoAutomata.estadoInicial=0;;
		
		ArrayList<ArrayList<Integer>> nuevosEstados = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> estadosNoMarcados = new ArrayList<ArrayList<Integer>>();
		
		ArrayList<Integer> estadoInicial = new ArrayList<Integer>();
		ArrayList<Integer> estado = new ArrayList<Integer>();
		
		estado.add(automata.estadoInicial);
		estadoInicial = cerraduraEps(automata,estado);
		nuevosEstados.add(estadoInicial);
		estadosNoMarcados.add(estadoInicial);
		
		while(!estadosNoMarcados.isEmpty()){
			estado = estadosNoMarcados.remove(estadosNoMarcados.size()-1);
			int estadoActual = contiene(nuevosEstados, estado);
			for(int n=0;n<alfabeto.size();n++){
				ArrayList<Integer> est = cerraduraEps(automata, mueve(automata, estado, alfabeto.get(n)));
				int index = contiene(nuevosEstados,est);
				if(index==-1){
					nuevosEstados.add(est);
					estadosNoMarcados.add(est);
					index = contiene(nuevosEstados, est);
				}
				if(index>-1)
					nuevoAutomata.addEdge(estadoActual, index, alfabeto.get(n));
			}
		}
		
		for(int n=0; n<nuevosEstados.size(); n++){
			estado = nuevosEstados.get(n);
			for(int m=0; m<automata.estadosFinales.size(); m++){
				if(estado.contains(automata.estadosFinales.get(m))){
					if(!nuevoAutomata.estadosFinales.contains(n))
						nuevoAutomata.estadosFinales.add(n);
				}
			}
		}
		
		return nuevoAutomata;
	}
	
	/**
	 * Indica el indice del conjunto si esta contenido en conjuntos
	 * @param conjuntos, conjunto de conjuntos
	 * @param conjunto, conjunto
	 * @return indice >= 0, -1 si no lo contiene, -2 si conjunto esta vacio
	 */
	private int contiene(ArrayList<ArrayList<Integer>> conjuntos, ArrayList<Integer> conjunto){
		if(!conjunto.isEmpty() && !conjuntos.isEmpty()){
			for(int n=0;n<conjuntos.size();n++){
				ArrayList<Integer> set = conjuntos.get(n);
				if(set.size()==conjunto.size()){
					int count = 0, index=n;
					for(int m=0;m<conjunto.size();m++)
						if(set.contains(conjunto.get(m)))count++;
					if(count==conjunto.size()) return index;
				}
			}return -1;
		}return -2;
		
	}
	
	/**
	 * Cerradura Epsilon
	 * @param automata, automata finito no determinista
	 * @param estados, conjunto
	 * @return cerradura, conjunto
	 */
	public ArrayList<Integer> cerraduraEps(Grafo automata, ArrayList<Integer> estados){
		if(estados!=null){
			ArrayList<Integer> pila = new ArrayList<Integer>();
			pila.addAll(estados);
			ArrayList<Integer> subpila = new ArrayList<Integer>();
			subpila.addAll(estados);
			
			while(!subpila.isEmpty()){
				int estado = subpila.remove(subpila.size()-1);
				LinkedList<Enlace<Integer>> edges = automata.getEdges(estado);
				if(edges!=null){
					for(int n=0;n<edges.size();n++){
						Enlace<Integer> e = edges.get(n);
						if(e.getPeso()==eps){
							if(!pila.contains(e.getDestino())){
								pila.add(e.getDestino());
								subpila.add(e.getDestino());
							}
						}
					} 
				}
			}
			return pila;
		}else return null;
	}
	
	/**
	 * Mueve
	 * @param automata, automata finito determinista
	 * @param estados, conjunto 
	 * @param transicion, (int)caracter
	 * @return mueve, conjunto
	 */
	public ArrayList<Integer> mueve(Grafo automata, ArrayList<Integer> estados, int transicion){
		if(estados!=null){
			ArrayList<Integer> pila = new ArrayList<Integer>();
			ArrayList<Integer> subpila = new ArrayList<Integer>();
			subpila.addAll(estados);
			while(!subpila.isEmpty()){
				int estado = subpila.remove(subpila.size()-1);
				LinkedList<Enlace<Integer>> edges = automata.getEdges(estado);
				if(edges!=null){
					for(int n=0;n<edges.size();n++){
						Enlace<Integer> e = edges.get(n);
						if(e.getPeso()==transicion){
							if(!pila.contains(e.getDestino())){
								pila.add(e.getDestino());
							}
						}
					} 
				}
			}
			return pila;
		}else return null;
	}
	
	/**
	 * Retorna el AFD de la clase
	 * @return AFD, Automata Finito Determinista
	 */
	public Grafo darAutomata(){
		return automata;
	}
}
