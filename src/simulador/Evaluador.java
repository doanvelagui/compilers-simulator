/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package simulador;

import java.io.*;
import java.util.*;


/**
 * Analizador de expresion regular r y cadena w
 * @author doanvelagui
 */
public class Evaluador {
	
	public final static int cat = 8226;
	public final static int pip = 124;
	public final static int ast = 42;
	public final static int eps = 248;
	
	/**
	 * Lee el archivo indicado y almacena en un ArrayList
	 * @param path, direccieon del archivo
	 * @param saveTo, Arraylist donde se almacena
	 * @param addSpaces, incluir espacios en blanco en la lectura
	 * @throws IOException, si existe error de lectura
	 */
	public void readChars(String path, ArrayList<Integer> saveTo, boolean addSpaces) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		int readed = br.read();
		while(readed!=-1){
			if(readed=='#') br.readLine(); //ignore comments
			else if(addSpaces){if (readed>=32) saveTo.add(readed);}
			else{if (readed>32) saveTo.add(readed);}
			readed = br.read();
		}
		br.close();
	}
	/**
	 * Define el alfabeto basado en la expresion regular 
	 * @param pila, donde se encuentra la epresion regular
	 * @param alfabeto, donde se almacena el alfabeto definido
	 */
	public void definirAlfabeto(ArrayList<Integer> pila, ArrayList<Integer> alfabeto){
		for (int n=0;n<pila.size();n++)
			if(pila.get(n)!='(' && pila.get(n)!=')' && pila.get(n)!=eps &&
					pila.get(n)!=pip && pila.get(n)!=ast &&
					pila.get(n)!=' ' && !alfabeto.contains(pila.get(n)))
				alfabeto.add(pila.get(n));
	}
	/**
	 * Construye un arbol basado en la expresion regular
	 * @param pila, caracteres leidos
	 * @param er, arbol de expresion regular
	 * @return root, arbol construido en base a la expresion regular 
	 */
	public ExpresionRegular expresionRegular(ArrayList<Integer> pila, ArrayList<Integer> alfabeto){
		if(pila.size()>1){
			int caracter = pila.remove(pila.size()-1);
			int nextChar = pila.get(pila.size()-1);
			ExpresionRegular root = new ExpresionRegular();
			
			//si caracter pertence a alfabeto o es epsilon
			if(alfabeto.contains(caracter)||caracter==eps){
				if(alfabeto.contains(nextChar) || nextChar==eps || nextChar==')' || nextChar==ast){
					root.cambiarSimbolo(cat);
					root.cambiarDerecha(new ExpresionRegular(caracter));
					root.cambiarIzquierda(expresionRegular(pila, alfabeto));
				}
				else if(nextChar==pip){
					int neNextChar = pila.get(pila.size()-2);
					if(alfabeto.contains(neNextChar) || neNextChar==eps || neNextChar==ast || neNextChar==')'){
						root.cambiarSimbolo(pip);
						ExpresionRegular derecha = new ExpresionRegular(pila.remove(pila.size()-1));
						derecha.cambiarDerecha(new ExpresionRegular(caracter));
						root.cambiarDerecha(new ExpresionRegular(caracter));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
				}
			}
			//si caracter es asterisco (Kleene)
			else if(caracter==ast){
				if(alfabeto.contains(nextChar)){
					if(pila.size()==1){
						root.cambiarSimbolo(ast);
						root.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
					}
					else{
						int neNextChar = pila.get(pila.size()-2);
						if(alfabeto.contains(neNextChar) || nextChar==eps || neNextChar==')' || neNextChar==ast){
							root.cambiarSimbolo(cat);
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
						else if(neNextChar==pip){
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
							root.cambiarSimbolo(pila.remove(pila.size()-1));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
					}
				}
				else if(nextChar==ast){
					root = expresionRegular(pila,alfabeto);
				}
				else if(nextChar==')'){
					pila.remove(pila.size()-1);
					ArrayList<Integer> subpila = subpila(pila);
					if(pila.isEmpty()){
						root.cambiarSimbolo(ast);
						root.cambiarIzquierda(expresionRegular(subpila, alfabeto));
					}
					else{
						nextChar = pila.get(pila.size()-1);
						if(alfabeto.contains(nextChar) || nextChar==eps || nextChar==ast || nextChar==')'){
							root.cambiarSimbolo(cat);
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(expresionRegular(subpila, alfabeto));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
						else if(nextChar==pip){
							root.cambiarSimbolo(pila.remove(pila.size()-1));
							ExpresionRegular derecha = new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(expresionRegular(subpila, alfabeto));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
					}
				}
			}
			//Si caracter es parentesis cerrado ')'
			else if(caracter==')'){
				ArrayList<Integer> subpila = subpila(pila);
				if(pila.isEmpty()){
					root = expresionRegular(subpila, alfabeto);
				}
				else{
					nextChar=pila.get(pila.size()-1);
					if(alfabeto.contains(nextChar) || nextChar==eps || nextChar==ast || nextChar == ')'){
						root.cambiarSimbolo(cat);
						root.cambiarDerecha(expresionRegular(subpila, alfabeto));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
					else if(nextChar==pip){
						root.cambiarSimbolo(pila.remove(pila.size()-1));
						root.cambiarDerecha(expresionRegular(subpila, alfabeto));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
				}
			}
			//TODO Si caracter es interrogacion
			
			
			return root;
		}
		else if(pila.size()==1)
			return new ExpresionRegular(pila.remove(pila.size()-1));
		else
			return null;
	}
	
	/**
	 * Crea una subpila basado en los parentesis
	 */
	private ArrayList<Integer> subpila(ArrayList<Integer> pila){
		int contador = 1;
		int index = -1;
		for (int m=pila.size()-1; contador!=0;m--){
			if(pila.get(m)==')')
				contador++;
			else if(pila.get(m)=='('){
				contador--;
				index=m;
			}
		}
		
		ArrayList<Integer> subpila = new ArrayList<Integer>();
		pila.remove(index);
		while(pila.size()>index)
			subpila.add(pila.remove(index));
		return subpila;
	}
	/**
	 * Verifica el numero de parentesis abiertos y cerrados
	 * @return true si es correcto, false si no lo es
	 * @param pila de caracteres leidos
	 */
	public boolean verificarParentesis(ArrayList<Integer> pila){
		if(pila.size()==0){
			System.err.println("Error en Expresion Regular: la expresion esta vacia");
			return false;
		}
		else{
			int parentesis = 0;
			for(int n=0; n<pila.size();n++){
				if (pila.get(n)==40)
					parentesis++;
				else if (pila.get(n)==41)
					parentesis--;
				if(parentesis<0){
					System.err.println("Error en Expresion Regular: Se cierran parentesis sin haber abierto");
					return false;
				}
			}
			if(parentesis!=0)
				System.err.println("Error en Expresion Regular: Hay parentesis abiertos y no se cierran");
			return parentesis==0;
		}
	}
	/**
	 * Verifica caracteres pipe
	 * @return true si es correcto, false si no lo es
	 * @param pila de caracteres leidos
	 */
	public boolean verificarPipe(ArrayList<Integer> pila){
		if(pila.size()==0){
			return false;
		}
		else{
			if(pila.get(0)==pip){
				System.err.println("Error en Expresion Regular: Caracter pipe al inicio");
				return false;
			}
			else if(pila.get(pila.size()-1)==pip){
				System.err.println("Error en Expresion Regular: Caracter pipe al final");
				return false;
			}
			for(int n=1; n<pila.size()-1;n++){
				if (pila.get(n)==124){
					if(pila.get(n-1)==pip || pila.get(n+1)==pip){
						System.err.println("Error en Expresion Regular: Dos caracteres pipe seguidos");
						return false;
					}
					else if(pila.get(n+1)==ast){
						System.err.println("Error en Expresion Regular: Caracter pipe seguido de asterisco");
						return false;
					}
					else if(pila.get(n-1)=='('){
						System.err.println("Error en Expresion Regular: Parentesis abierto seguido de caracter pipe");
						return false;
					}
					else if(pila.get(n+1)==')'){
						System.err.println("Error en Expresion Regular: Caracter pipe seguido parentesis cerrado");
						return false;
					}
				}
			}
			return true;
		}
	}
	/**
	 * Verifica caracteres asterisco
	 * @return true si es correcto, false si no lo es
	 * @param pila de caracteres leidos
	 */
	public boolean verificarAsterisco(ArrayList<Integer> pila){
		if(pila.size()==0){
			return false;
		}
		else{
			if(pila.get(0)==ast){
				System.err.println("Error en Expresion Regular: Caracter asterisco al inicio");
				return false;
			}
			for(int n=1; n<pila.size();n++){
				if(pila.get(n)==ast){
					if(pila.get(n-1)=='('){
						System.err.println("Error en Expresion Regular: Parentesis abierto seguido de asterisco");
						return false;
					}
				}
			}
			return true;
		}
	}
}