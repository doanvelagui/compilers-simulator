/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package simulador;

public class ExpresionRegular {
	
	private ExpresionRegular izquierda;
	private ExpresionRegular derecha;
	private int simbolo;
	
	/**
	 * Constructor
	 */
	public ExpresionRegular(){
		izquierda = null;
		derecha = null;
	}
	/**
	 * Constructor
	 * @param simbolo asociado al nodo
	 */
	public ExpresionRegular(int simbolo){
		this();
		if(simbolo>32)
			this.simbolo = simbolo;
	}
	/**
	 * Cambia el valor del nodo izquierdo
	 * @param izquierda, nuevo nodo izquierdo
	 */
	public void cambiarIzquierda(ExpresionRegular izquierda){
		this.izquierda = izquierda;
	}
	/**
	 * Cambia el valor del nodo derecho
	 * @param derecha, nuevo nodo derecho
	 */
	public void cambiarDerecha(ExpresionRegular derecha){
		this.derecha = derecha;
	}
	/**
	 * Cambia el simbolo asociado al nodo.
	 * @param simbolo, nuevo valor para el nodo
	 * @return true si cambia el valor, false si no lo hace.
	 */
	public boolean cambiarSimbolo(int simbolo){
		if(simbolo>32)
			this.simbolo = simbolo;
		return (simbolo>32) ? true: false;
	}
	/**
	 * Retorna el nodo izquierdo
	 * @return nodo izquierdo
	 */
	public ExpresionRegular darIzquierda(){
		return izquierda;
	}
	/**
	 * Retorna el nodo derecho
	 * @return nodo derecho
	 */
	public ExpresionRegular darDerecha(){
		return derecha;
	}
	/**
	 * Retorna el simbolo asociado al nodo
	 * @return simbolo
	 */
	public int darSimbolo(){
		return simbolo;
	}
	
	
	
	
	/**
	 * Recorre el arbol inorden.
	 * @return expresion regular
	 */
	public String toStringInorden(){
		String arbol = "";
		if(izquierda!=null)
			arbol += izquierda.toStringInorden();
		arbol += (char)(int)simbolo;
		if(derecha!=null)
			arbol += derecha.toStringInorden();
		return arbol;
	}
	
	public String toStringPreorden(){
		String arbol = "";
		arbol += (char)(int)simbolo;
		if(izquierda!=null)
			arbol += izquierda.toStringPreorden();
		if(derecha!=null)
			arbol += derecha.toStringPreorden();
		return arbol;
	}
}
