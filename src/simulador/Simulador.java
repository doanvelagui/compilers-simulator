/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package simulador;

import grafo.*;

import java.io.*;
import java.util.*;

public class Simulador {
		
	// VARIABLES
	protected ArrayList<Integer> cadena;
	protected ArrayList<Integer> alfabeto;
	protected ArrayList<Integer> pila;
	protected ExpresionRegular expresionRegular;
	protected ExpresionRegular expresionRegularExtendida;
	protected Evaluador evaluador;
	protected AFN afn;
	protected AFD afd;
	protected AFD_Directo afd2;

	// METODOS
	
	/**
	 * Constructor
	 */
	public Simulador(){
		cadena = new ArrayList<Integer>();
		pila = new ArrayList<Integer>();
		alfabeto = new ArrayList<Integer>();
		evaluador = new Evaluador();
		expresionRegular = new ExpresionRegular();
	}
	
	/**
	 * Lee los archivos data/expresionRegular.txt y data/cadena.txt
	 */
	private void lectura(){
		try{ //Lectura de expresion regular
			evaluador.readChars("data/expresionRegular.txt", pila, false);
		}catch (IOException ioe){
			System.err.print("Error al leer expresionRegular.txt");
		}
		try{ //Lectura de cadena
			evaluador.readChars("data/cadena.txt", cadena, true);
		}catch (IOException ioe){
			System.err.print("Error al leer cadena.txt");
		}
	}
	
	/**
	 * Construccion de estructuras 
	 */
	private void construir(){
		//construir arbol asociado a la expresion regular
		if(evaluador.verificarParentesis(pila)&&evaluador.verificarPipe(pila)&&evaluador.verificarAsterisco(pila)&&!pila.isEmpty()){
			evaluador.definirAlfabeto(pila, alfabeto);
			expresionRegular = evaluador.expresionRegular(pila, alfabeto);
			expresionRegularExtendida = new ExpresionRegular('�');
			expresionRegularExtendida.cambiarDerecha(new ExpresionRegular('#'));
			expresionRegularExtendida.cambiarIzquierda(expresionRegular);
		}
		afn = new AFN(expresionRegular,alfabeto);
		afd = new AFD(afn,alfabeto);
		afd2 = new AFD_Directo(expresionRegularExtendida, alfabeto);
	}
	
	/**
	 * Escribe el automata finito recibido en el archivo indicado en el path
	 * @param automata, grafo representativo del automata
	 * @param path, localizacion del archivo
	 */
	public void escribirAF(Grafo automata, String path){
		try{
			PrintWriter out = new PrintWriter(new File(path));
			Integer[] estados = automata.getNodos();
			out.println("digraph finite_state_machine {");
			out.println("\trankdir=LR;");
			out.println("\tsize=\"12\"");
			out.print("\tnode [shape=doublecircle]; "+automata.estadoInicial+" ");
			for(int n=0;n<automata.estadosFinales.size();n++)
				out.print(automata.estadosFinales.get(n)+" ");
			out.println(";");
			out.println("\tnode [shape=circle];");
			for( int n=0; n<estados.length;n++){
				LinkedList<Enlace<Integer>> edges = automata.getEdges(estados[n]);
				while(edges!=null && !edges.isEmpty()){
					Enlace<Integer> e = edges.remove();
					String label = (e.getPeso()==Evaluador.eps) ? "eps" : ""+(char)(int)e.getPeso();
					out.println("\t"+estados[n]+" -> "+e.getDestino()+" [label=\""+label+"\"];");
				}
			}
			out.println("}");
			out.close();
		}catch(IOException ioe){
			System.err.println("Error en escritura de AFN");
		}
	}
	
	/**
	 * Simulacion de un AFD
	 * @param automata representativo de AFD
	 * @param estadoInicial del AFD
	 * @param estadosFinales del AFD
	 * @param cadena leida de cadena.txt
	 * @param alfabeto generado en base a la expresion regular
	 * @return 1 si cadena pertenece al lenguage generado por el AFD, 0 si no pertenece.
	 */
	public int SimulacionD(Grafo automata, int estadoInicial, ArrayList<Integer> estadosFinales, 
			ArrayList<Integer> cadena, ArrayList<Integer> alfabeto){	
		int estado = estadoInicial;
		pila.addAll(cadena);
		while(!pila.isEmpty()){
			int caracter = pila.remove(0);
			LinkedList<Enlace<Integer>> edges = automata.getEdges(estado);
			for(int m=0;m<edges.size() ;m++){
				Enlace<Integer> edge = edges.get(m);
				if(edge.getPeso()==caracter){
					estado=edge.getDestino();
				}
			}
		}
		return (estadosFinales.contains(estado)) ? 1 : 0;
	}
	
	/**
	 * Simulacion de un AFN
	 * @param automata representativo de AFN
	 * @param estadoInicial del AFN
	 * @param estadosFinales del AFN
	 * @param cadena leida de cadena.txt
	 * @param alfabeto generado en base a la expresion regular
	 * @return 1 si cadena pertenece al lenguage generado por el AFN, 0 si no pertenece.
	 */
	public int SimulacionN(Grafo automata, int estadoInicial, ArrayList<Integer> estadosFinales, 
			ArrayList<Integer> cadena, ArrayList<Integer> alfabeto){	
		ArrayList<Integer> estados = new ArrayList<Integer>();
		estados.add(estadoInicial);
		estados = afd.cerraduraEps(automata, estados);
		
		pila.addAll(cadena);
		while(!pila.isEmpty()){
			int caracter = pila.remove(0); 
			ArrayList<Integer> ests = afd.cerraduraEps(automata, afd.mueve(automata, estados, caracter));
			estados = new ArrayList<Integer>();
			estados.addAll(ests);
		}
		
		for(int n=0;n<estados.size();n++){
			if(estadosFinales.contains(estados.get(n)))
				return 1;
		}	
		return 0;
	}
	
	/**
	 * Corre simulador
	 */
	public void run(){
		lectura();
		construir();
		
		System.out.println("AFN generado por algoritmo de Thompson");
		System.out.println((System.currentTimeMillis()));
		System.out.println(SimulacionN(afn.automata, afn.automata.estadoInicial, afn.automata.estadosFinales, cadena, alfabeto));
		System.out.println((System.currentTimeMillis()));
		System.out.println();
		System.out.println("AFD generado por subconjuntos");
		System.out.println((System.currentTimeMillis()));
		System.out.println(SimulacionD(afd.automata, afd.automata.estadoInicial, afd.automata.estadosFinales, cadena, alfabeto));
		System.out.println((System.currentTimeMillis()));
		System.out.println();
		System.out.println("AFD generado por algoritmo directo");
		System.out.println((System.currentTimeMillis()));
		System.out.println(SimulacionD(afd2.automata, afd2.automata.estadoInicial, afd2.automata.estadosFinales, cadena, alfabeto));
		System.out.println((System.currentTimeMillis()));
		
		if(afn!=null)
			escribirAF(afn.automata, "out/AFN.dot");
		if(afd!=null)
			escribirAF(afd.automata, "out/AFD.dot");
		if(afd2!=null)
			escribirAF(afd2.automata, "out/AFD2.dot");
	}
	
	/**
	 * Main
	 * @param args
	 */
	public static void main(String args[]){
		Simulador s = new Simulador();
		s.run();
	}
}
